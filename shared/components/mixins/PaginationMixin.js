import { isEqual } from 'lodash'
import { sanitizePaginationParams } from 'shared/utils'

export default {
  data () {
    return {
      search: null,
      loading: false,
      pageApiUrl: '#', // overidden by the page
      basePage: '/', // overidden by the page
    }
  },

  computed: {
    numberOfPages () {
      let itemsPerPage = this.itemsPerPage && parseInt(this.itemsPerPage) || process.env.itemsPerPage
      return Math.ceil(this.total / itemsPerPage)
    },

    isFirstPage () {
      return this.page == 1
    },
  },

  methods: {
    showFirstPage () {
      this.$router.push({path: this.basePage})
    },

    onPaginationOptionChange () {
      if(this.page == 1){
        this.reloadData()
      } else {
        this.showFirstPage()
      }
    },

    async reloadData () {
      this.loading = true
      try {
        let options = this.apiQueryParams()
        let { items, total } = await this.$axios.$get(this.pageApiUrl, { params: options })
        this.items = items
        this.total = total
        this.syncLocalPagData(options)
      } catch (e){
        this.$nuxt.context.error(e)
      } finally {
        this.$nextTick(function () { // to avoid trigger pagination options (like 'itemsPerPage', etc) watchers, wait until nextTick
          this.loading = false
        })
      }
    },

    syncLocalPagData (options) {
      this.page = options.page
      this.itemsPerPage = options.itemsPerPage
      this.sortBy = options.sortBy
      this.sortDesc = options.sortDesc
    },

    apiQueryParams () {
      let options = { ...this.currentPagination, page: this.$route.params.page } // copy it to avoid directly mutations and set page from page params
      return sanitizePaginationParams(options, process.env.itemsPerPage, process.env.maxItemsPerPage)
    },
  },



  beforeMount () {
    if(!this.staticPagination || !this.currentPagination) throw new Error("staticPagination or currentPagination not provided")
    if(!isEqual(this.staticPagination, this.currentPagination)) {
      this.reloadData()
    }
  },

  watch: {
    'itemsPerPage': function  (val) {
      if(this.loading) return
      this.updatePaginationOptions({itemsPerPage: val})
      this.onPaginationOptionChange()
    },

    'sortBy': function  (val) {
      if(this.loading) return
      this.updatePaginationOptions({sortBy: val})
      this.onPaginationOptionChange()
    },

    'sortDesc': function  (val) {
      if(this.loading) return
      this.updatePaginationOptions({sortDesc: val})
      this.onPaginationOptionChange()
    },
  }
}
