// ColorsMixin

export default {
  computed: {
    theme () {
      return this.$vuetify.theme.themes.dark ? this.$vuetify.theme.themes.dark : this.$vuetify.theme.themes.light
    },

    isDarkTheme () {
      return this.$vuetify.theme.themes.dark
    },

    isLightTheme () {
      return !this.isDarkTheme
    },

    colorInfo () {
      return this.theme.info
    },

    colorSuccess () {
      return this.theme.success
    },

    colorWarning () {
      return this.theme.warning
    },

    colorError () {
      return this.theme.error
    }

  }
}
