// ErrorMixin

export default {
  data: function () {
    return {
      error: '',
    }
  },

  methods: {
    clearError () {
      if(this.error){
        this.error = ''
      }
    },

    setError (error) {
      this.error = error
    },

    showErrorPage (errObj = {}) {
      let { response: { status = 500 }, response: {data: { message = "An error occured"}}} = errObj
      console.log('status %s', status)
      console.log('message %s', message)
      this.$nuxt.context.error({ statusCode: status, message: message })
    },
  },
}
