const { PRODUCT_TYPE } = require('./product')

const ITEM_TYPE = {
  SIMPLE: PRODUCT_TYPE.SIMPLE,
  VARIABLE: PRODUCT_TYPE.VARIABLE,
  VARIATION: PRODUCT_TYPE.VARIATION,
}

const CartableItem = class {
  constructor (args = {}) {
    let { _id, name, sku, itemType = ITEM_TYPE.SIMPLE, image, slug, price, regularPrice, volumePrice, description, data } = args
    Object.assign(this, {_id, name, sku, itemType, image, slug, price, regularPrice, volumePrice, description, data })
  }
}

module.exports = {
  ITEM_TYPE,
  CartableItem,
}
