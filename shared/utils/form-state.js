// ====================== FORM STATE ======================
const FormState = {
  NEW: 'new',
  EDIT: 'edit',
}

const isNewForm = (val) => {
  return val == FormState.NEW
}

const isEditForm = (val) => {
  return val == FormState.EDIT
}

// const parseErrorObj = (errObj) => {
//   let { response: { status = 500 }, response: {data: { message = "An error occured"}}} = errObj
//   return { statusCode: status, message: message }
// }

module.exports = {
  FormState,
  isNewForm,
  isEditForm,
}
