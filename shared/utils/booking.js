const BookingService = class {
  constructor (args = {}) {
    let { name, duration, sku, bufferBefore = 0, bufferAfter = 0, price, currency } = args
    this.name = name
    this.sku = sku
    this.duration = parseInt(duration) || 0
    this.bufferBefore = parseInt(bufferBefore) || 0
    this.bufferAfter = parseInt(bufferAfter) || 0
    this.price = price
    this.currency = currency
  }

  get totalDuration () {
    return this.duration + this.bufferBefore + this.bufferAfter
  }
}

const Booking = class extends BookingService {
  constructor (args = {}) {
    super(args)
    let { id } = args
    this.id = id
    // this.sku = sku
    // this.duration = parseInt(duration)
    // this.bufferBefore = parseInt(bufferBefore) || 0
    // this.bufferAfter = parseInt(bufferAfter) || 0
    // this.price = price
    // this.currency = currency
  }
}


const TimeBlockConfig = class {
  constructor (from, to, sessionFrom, sessionTo, disabled = false) {
    if(!from || !to) throw new Error ('Block time From and To shold be provided')
    this.from = from
    this.to = to
    this.sessionFrom = sessionFrom || from
    this.sessionTo = sessionTo || to
    this.disabled = disabled
  }
}



module.exports = {
  BookingService,
  TimeBlockConfig,
  Booking,
}
