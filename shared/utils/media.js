const ImageKit = require('imagekit-javascript')


// -------------------- ImageKit --------------------------

const imagekit = new ImageKit({
    publicKey : process.env.IMGKIT_PUBLIC_KEY,
    urlEndpoint : process.env.IMGKIT_ENDPOINT,
    authenticationEndpoint : process.env.API_URL + process.env.IMGKIT_LOCAL_SERVER_AUTH_PATH,
})

const imgUrl = function (path, transformation = []) {
  return imagekit.url({path, transformation})
}

const imgResp = function (path, args = []) {
  let img = {}
  const url = imgUrl(path)

  img.srcset = args.map(e => {
    let src = null
    if(typeof e === 'object') {
      if(!e.w) return
      const transformation = e.tr ? e.tr : [{ width: e.w }]
      src = `${imgUrl(path, transformation)} ${e.w}w`
    } else if (typeof e === 'string' || typeof e === 'number') {
      src = `${imgUrl(path, [{width: e}])} ${e}w`
    }
    return src
  }).join(', ')

  img.sizes = args.map(e => {
    if(typeof e === 'object') {
      return e.w ? `(max-width: ${e.screen ? e.screen : e.w}px) ${e.w}px` : null
    } else if (typeof e === 'string' || typeof e === 'number') {
      return `${e}px`
    }
  }).join(', ')

  img.url = url
  return img
}



const splitArrayInChunks = function (arr, chunkSize = 5) {
  if(!Array.isArray(arr)) {  throw new Error("Array expected as first argument") }
  if(!arr.length || chunkSize <= 1) return arr

  let splitted = []
  let chunks = Math.ceil(arr.length/chunkSize)
  for (let i = 0; i < chunks; i++) {
    splitted[i] = arr.slice(i*chunkSize, (i+1)*chunkSize)
  }
  return splitted
}

const promisifyImagekitUpload = function (data) {
  return new Promise((result, reject) => {
    imagekit.upload(data, (err, res) => {
      if(err) return reject(err)
      result(res)
    })
  })
}

const fileUpload = function (data) {
  return promisifyImagekitUpload(data)
}

// it will upload a file and will return an object with {error: uploadError, data: uploadResult} for use in Promise.all so it doea not stop it when an error in encountered
const safeUpload = async function (data) {
  let res = {}
  try {
    res.data = await promisifyImagekitUpload(data)
  } catch (error) {
    res.error = error
  }
  return res
}




module.exports = {
  imagekit,
  imgUrl,
  imgResp,
  splitArrayInChunks,
  fileUpload,
  safeUpload,
}
