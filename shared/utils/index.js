// const Vue = require('vue')

const jsonCopy = (val) => {
    return JSON.parse(JSON.stringify(val))
}

// return val if it is an Int > 0 or the default value
const safeNatNumber = function (val, defaultVal = 1) {
  let n = parseInt(val)
  return n && n > 0 ? n : defaultVal
}

// it will convert 'false' or '0' string as false boolean
// const safeBool = function (val) {
//   if(val == null) return false
//   if('boolean' === typeof val) return val
//   return ['false', '0'].indexOf(('' + val).toLowerCase()) != -1 ? false : Boolean(val)
// }

// filter an object properties based on predicate function
const filterObject = (obj, predicate) => {
  if(!obj || !obj.__proto__ || obj.__proto__.constructor.name !== 'Object'){
    throw new Error("The first argument should be an Object")
  }

  let newObj = {}
  for(let prop in obj){
    if(Object.prototype.hasOwnProperty.call(obj, prop) && predicate(obj[prop], prop, obj)){
      newObj[prop] = obj[prop]
    }
  }

  return newObj
}

const arrayContainsDuplicates = (arr, caseInsensitive) => {
  if(!Array.isArray(arr)) throw new Error("Array expected")
  let copy = caseInsensitive ? arr.map(e => ('' + e).toLowerCase()) : arr
  for(let i = 0; i < copy.length; i++){
    if(copy.indexOf(copy[i]) != i) return true
  }

  return false
}

const parseErrorObj = (errObj) => {
  let { response: { status = 500 }, response: {data: { message = "An error occured"}}} = errObj
  return { statusCode: status, message: message }
}

// emit events from the provided component
const pipeUp = function (comp, eventsArr) {
  // if(!comp || !(comp instanceof Vue)){
  //   throw new Error ("A Vue instance should be provided as emmiter component.")
  // }

  return eventsArr.reduce((acc, e) => {
    acc[e] = (val) => { comp.$emit(e, val) }
    return acc
  },
  {})
}


function sanitizeItemsPerPageParam (val, defaultVal = 10, max = 100) {
  let items = safeNatNumber(val, defaultVal)
  return items > 0  && items <= max ? items : defaultVal
}

function sanitizePaginationParams (options, defaultItemsPerPage, maxItemsPerPage) {
  return Object.assign({}, options, { page: safeNatNumber(options.page), itemsPerPage: sanitizeItemsPerPageParam(options.itemsPerPage, defaultItemsPerPage, maxItemsPerPage)})
}


module.exports = {
  jsonCopy,
  safeNatNumber,
  filterObject,
  arrayContainsDuplicates,
  parseErrorObj,
  pipeUp,
  sanitizePaginationParams,

  ... require('./booking'),

  ... require('./cart'),

  ... require ('./date-and-time'),

  ... require ('./form-state'),

  ... require ('./media'),

  ... require('./product'),

  ... require ('./filters'),
}
