const isEqual = require('lodash/isEqual.js')

// ====================== PRODUCT TYPES ======================
const PRODUCT_TYPE = {
  SIMPLE: 'simple',
  VARIABLE: 'variable',
  VARIATION: 'variation',
}

const PRODUCT_TYPE_NAME = {
  SIMPLE: 'Simple',
  VARIABLE: 'Variable',
  VARIATION: 'Variation',
}

const PRODUCT_GROUP = {
  PLAN: 'plan',
  BOOKLET: 'booklet',
}

const productTypeName = function (type) {
  return PRODUCT_TYPE_NAME[type]
}

const productTypes = function () {
  return Object.keys(PRODUCT_TYPE_NAME).map(e => ({text: PRODUCT_TYPE_NAME[e], value: e }))
}

const isSimpleProduct = function (product) {
  return product && product.type == PRODUCT_TYPE.SIMPLE
}

const isVariableProduct = function (product) {
  return product && product.type == PRODUCT_TYPE.VARIABLE
}

const isProductVariation = function (product) {
  return product && product.type == PRODUCT_TYPE.VARIATION
}

// ==================== product ====================
const variationByConfig = function (variations, config) {
  return variations.find(e => isEqual(e.featuresConfig, config))
}

// object comparation for variation featuresConfig
const sameFeaturesConfig = function (obj1, obj2) {
  return isEqual(obj1, obj2)
}

// generate all possible variations from a product.featuresConfig
const allVariationsFeaturesConfig = function (productVariableFatures) {
  return productVariableFatures.reduce((acc, e) => {
    let configs = e.items.map(vfItem => ({[e.slug]: vfItem.slug}))
    return combineArrObj(acc, configs)
  }, [{}])
}

// return an array from combining objects from firstArr with the ones in secondArr
function combineArrObj (firstArr, secondArr) {
  return firstArr.reduce((acc, e) => {
    return [...acc, ...secondArr.map(f => Object.assign({}, e, f))]
  }, [])
}


// return the variation configuration as an array of objects { featureName, featureSlug, itemName, itemSlug, itemValue, itemDescription}}
const fullVariationConfig = (variationConfig, variableFeatures) => {
  if(!variationConfig || !Array.isArray(variableFeatures)) return []
  // variableFeatures dictate the order of the features
  return variableFeatures.reduce((acc, vf) => {
    let { name: featureName, slug: featureSlug } = vf
    let featureValue = variationConfig[featureSlug]
    if(featureValue) {
      let item = vf.items.find(e => e.slug == variationConfig[featureSlug])
      if(item) {
        let { name: itemName, slug: itemSlug, value: itemValue, description: itemDescription } = item
        acc.push({ featureName, featureSlug, itemName, itemSlug, itemValue, itemDescription })
      }
    }
    return acc
  }, [])
}

const vfBySlug = (featureSlug, variableFeatures) => {
  if(!featureSlug || !Array.isArray(variableFeatures)) return
  return variableFeatures.find(vf => vf.slug == featureSlug)
}

const vfItemBySlug = (itemSlug, variableFeature) => {
  if(!itemSlug || !variableFeature || !Array.isArray(variableFeature.items)) return
  return variableFeature.items.find(vf => vf.slug == itemSlug)
}

const extractVFItem = (itemSlug, featureSlug, variableFeatures) => {
  if(!itemSlug || !featureSlug || !Array.isArray(variableFeatures)) return
  let vf = vfBySlug(featureSlug, variableFeatures)
  return vfItemBySlug(itemSlug, vf)
}

const variationDescription = (product) => {
  const config = fullVariationConfig(product.featuresConfig, product.variableFeatures)
  return config.map(e => `${e.featureName} ${e.itemName}`).join(', ')
}

const featureConfigAsString = (featuresConfig) => {
  return Object.keys(featuresConfig).map(e => e + '-' + featuresConfig[e]).join('|')
}

const lessExpenciveVariationPrice = (product) => {
  if(!isVariableProduct(product)) return null
  let price = product.price
  let regularPrice = product.regularPrice
  product.variations.map(e => {
    if(e.price && e.price < price) {
      price = e.price
      regularPrice = e.regularPrice
    }
  })

  return { price, regularPrice }
}

function volumePriceStringToArr (priceStr) {
  if(!priceStr || typeof priceStr !== 'string') return []

  const pricesArr = priceStr.split(',').reduce((acc, e) => {
    let [qty, price] = e.split(':').map(e => parseFloat(e))
    if(qty && price) {
      acc.push({qty, price})
    }
    return acc
  }, [])

  return pricesArr.sort((a,b) => a.qty - b.qty)
}

function productPriceForQty (singlePrice, qty = 1, volumePriceArr) {
  if(!volumePriceArr || !Array.isArray(volumePriceArr) || !volumePriceArr.length || qty < 2) return singlePrice
  return volumePriceArr.reduce((acc, e) => {
    if(e.qty <= qty) {
      acc = e.price
    }
    return acc
  }, singlePrice)
}





module.exports = {
  PRODUCT_TYPE,
  PRODUCT_TYPE_NAME,
  PRODUCT_GROUP,
  productTypeName,
  productTypes,
  isSimpleProduct,
  isVariableProduct,
  isProductVariation,
  variationByConfig,
  sameFeaturesConfig,
  allVariationsFeaturesConfig,
  fullVariationConfig,
  variationDescription,
  lessExpenciveVariationPrice,
  featureConfigAsString,
  extractVFItem,

  volumePriceStringToArr,
  productPriceForQty,
}
