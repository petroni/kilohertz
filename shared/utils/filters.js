const formatPrice = function (price, currency = 'CHF', locale = 'de-CH') {
  if(!price) return ''
  const minimumFractionDigits =  Math.round(price) == +price  ? 0 : 2
  return new Intl.NumberFormat(locale, { style: 'currency', currency: currency, minimumFractionDigits }).format(price)
}

const formatSurface  = (val, unit = 'm') => {
  if(val == null || val == '') return ''
    return `${val} ${unit}<sup>2</sup>`
}

module.exports = {
  formatPrice,
  formatSurface,
}
