const dayjs = require('dayjs')
const updateLocale = require('dayjs/plugin/updateLocale')
const isoWeek = require( 'dayjs/plugin/isoWeek')
const localeData = require( 'dayjs/plugin/localeData')
const isBetween = require( 'dayjs/plugin/isBetween')
const relativeTime = require( 'dayjs/plugin/relativeTime')
const isSameOrBefore = require( 'dayjs/plugin/isSameOrBefore')

const utc = require( 'dayjs/plugin/utc')
const timezone = require( 'dayjs/plugin/timezone')

dayjs.extend(utc)
dayjs.extend(timezone)



dayjs.extend(updateLocale)
dayjs.extend(isoWeek)
dayjs.extend(localeData)
dayjs.extend(isBetween)
dayjs.extend(relativeTime)
dayjs.extend(isSameOrBefore)

dayjs.tz.setDefault("Europe/Paris")

const formatDateToLocale = function (dateAsString, locale = 'en-US', options = { year: 'numeric', month: 'short', day: 'numeric' }) {
  try{
    let d = new Date(dateAsString)
    return d.toLocaleDateString(locale, options)
  } catch(error) {
    console.log('error')
  }
  return ''
}

const formatDateTimeToLocale = function (dateAsString, locale = 'en-US', options = { year: 'numeric', month: 'short', day: 'numeric', hour: '2-digit', minute:'2-digit' }) {
  try{
    let d = new Date(dateAsString)
    return d.toLocaleDateString(locale, options)
  } catch(error) {
    console.log('error')
  }
  return ''
}

const formatDate = (date, dateFormat = "YYYY-MM-DD") => {
  return date ? dayjs(date).format(dateFormat) : ''
}

const formatTime = (date, format = 'HH:mm') => {
  return date ? dayjs(date).format(format) : ''
}

// day 1 - Monday, ... 7 - Sunday
const isoWeekday = (day, format = 'dddd') => {
  return dayjs().isoWeekday(day).format(format)
}

// modify dayjs().day() so that 0 is Monday, ... 6 is Sunday (default is Sunday 0, Monday 1, ...)
const weekdayNumber = (date) => {
  return (dayjs(date).day()+6)%7
}

const timeblocksOverlap = (t1, t2) => {
  return dayjs(t2.from).isBetween(t1.from, t1.to, null, '[)') || dayjs(t2.to).isBetween(t1.from, t1.to, null, '(]')
}

const timeAdd = (date, value, unit='minute') => {
  return dayjs(date).add(value, unit).toISOString()
}

const timeSubstract = (date, value, unit='minute') => {
  return dayjs(date).subtract(value, unit).toISOString()
}

const sameTimeblock = (b1, b2) => {
  return b1 && b2 && b1.from == b2.from && b1.to == b2.to
}

// date is YYYY-MM-DD format, start & end is the hour:min format and duration is given in minutes
// return an array of objects [{s: ISODate, e: ISODate}, ...]
const splitDateTimeInterval = (date, start, end, duration) => {
  if(!start || !end) return []
  let startInterval = dayjs(date).hour(start.split(':')[0]).minute(start.split(':')[1])
  let endInterval = dayjs(date).hour(end.split(':')[0]).minute(end.split(':')[1])
  let intervals = []
  while(startInterval.add(duration, 'minute').isSameOrBefore(endInterval)) {
    intervals.push({from: startInterval.toISOString(), to:startInterval.add(duration, 'minute').toISOString()})
    startInterval = startInterval.add(duration, 'minute')
  }
  return intervals
}


const dailySumWorkingHours = (weeklySchedule, unit = 'hour') => {
  if(!Array.isArray(weeklySchedule.items)) throw new Error("Array expected")
  return weeklySchedule.items.reduce((acc, e) => {
    const dailyTime = e.reduce((timeAcc, slot) => {
      if(!slot.from || !slot.to) return timeAcc
      let [fromHour = 0, fromMin = 0] = slot.from.split(':')
      let [toHour = 0, toMin = 0] = slot.to.split(':')
      timeAcc += dayjs().hour(toHour).minute(toMin).diff(dayjs().hour(fromHour).minute(fromMin), unit)
      return timeAcc
    }, 0)
    acc.push(dailyTime)
    return acc
  }, [])
}

// return the sum of timeblocks
const sumTimeBlocks = (arrOfBlocks, unit = 'hour') => {
  if(!arrOfBlocks || !arrOfBlocks.length) return 0
  return arrOfBlocks.reduce((acc, e) => {
    acc += dayjs(e.to).diff(e.from, unit)
    return acc
  }, 0)
}


const datesWorkingHours = (datesArr, weeklySchedule, unit) => {
  if(!datesArr || !Array.isArray(datesArr) || !weeklySchedule) return [[]]
  let dailySum = dailySumWorkingHours(weeklySchedule) // 0 is Monday, ...
  return datesArr.map(e => {
    //console.log('e %o %o', e, weekdayNumber(e))
    return dailySum[weekdayNumber(e)] // because schedule is stored as Monday first
  })
}


module.exports = {
  formatDateToLocale,
  formatDateTimeToLocale,
  formatDate,
  formatTime,
  isoWeekday,
  weekdayNumber,
  timeblocksOverlap,
  timeAdd,
  timeSubstract,
  sameTimeblock,
  splitDateTimeInterval,
  sumTimeBlocks,
  datesWorkingHours,
}
