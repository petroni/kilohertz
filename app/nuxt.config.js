const path = require('path')
import colors from 'vuetify/es5/util/colors'
const axios = require('axios')

export default {
  // "srcDir": __dirname,
  // "buildDir": "_nuxt/app",
  // "lambdaName": "index", // main app should be index, not needed here
  //
  //
  //
  // target: 'server',

  env: {
    ITEMS_PER_PAGE: process.env.ITEMS_PER_PAGE || 10,
    MAX_ITEMS_PER_PAGE: process.env.MAX_ITEMS_PER_PAGE || 100,
    API_URL: process.env.API_URL,

    USER_REGISTER_ENDPOINT_URL: process.env.USER_REGISTER_ENDPOINT_URL || process.env.API_URL +' /users',

    IMGKIT_PUBLIC_KEY: 'public_+9disIZyM7bZUFcu7H1CfrIcSsU=',
    IMGKIT_ENDPOINT: 'https://ik.imagekit.io/kilohertz',
    IMGKIT_LOCAL_SERVER_AUTH_PATH: '/api/auth-to-imagekit',
  },

  publicRuntimeConfig: {
    stripePublishableKey: process.env.STRIPE_PUBLISHABLE_KEY || 'pk_test_51HtFaWGGe0aXJVZpqZsBElJG3kCujuzqvvH7MmsbAgnzalz5baGT7ONXRCY40T0oj0N94YdaHOHJWL8VC1OXsf9D00CYlYbK1P',
    checkoutSuccessUrl: process.env.SITE_URL + '/payment/success',
    checkoutCancelUrl: process.env.SITE_URL  + '/payment/canceled',
  },

  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    titleTemplate: '%s - Kilohertz',
    title: 'Kilohertz',
    htmlAttrs: {
      lang: 'de'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' }
    ],

    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
    ],

    script: [
      // {
      //   src: "https://www.googletagmanager.com/gtag/js?id=G-VMY0TTNRD4",
      //   async: true,
      // },
      //
      // {
      //   src: "/google-analytics.js",
      // },
    ],
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    '~/assets/css/main.scss',
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    "~/plugins/vee-validate.js",
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: {
    dirs: [
      '~/components',
      '~/../shared/components',
    ]
  },

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/vuetify
    '@nuxtjs/vuetify',
    '@nuxtjs/google-fonts',
    // '@nuxtjs/google-analytics',
  //  '@nuxtjs/eslint-module',
      '@nuxtjs/sitemap',
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
    '@nuxtjs/auth',
  ],

  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {
    proxy: true,
    headers: {
      "Access-Control-Allow-Origin": "*",
      "Access-Control-Allow-Credentials": true,
      "Access-Control-Allow-Methods": "GET,OPTIONS,PATCH,DELETE,POST,PUT" ,
      "Access-Control-Allow-Headers": "X-CSRF-Token, X-Requested-With, Accept, Accept-Version, Content-Length, Content-MD5, Content-Type, Date, X-Api-Version"
    },
  },

  generate: {
    fallback: true
  },

  proxy: {
    '/api': {
      target: process.env.API_URL,
    },
  },

  sitemap: {
    hostname: 'https://kilohertz.ch',
    exclude: [
      '/cart',
    ],

    routes: async () => {
      const pp = await Promise.all([
        axios.get(`api/all-products-slugs`),
        axios.get(`api/all-posts-slugs`)
      ])

      return [
        ...pp[0].data.map(e => `/plan/${e.slug}`),
        ...pp[1].data.map(e => `/post/${e.slug}`),
      ]
    }
  },

  auth: {
    strategies: {
      local: {
        endpoints: {
          login: { url: '/api/auth/login', method: 'post', propertyName: 'token' },
          logout: false,
          user: { url: '/api/auth/me', method: 'get', propertyName: 'user' },
          //autoFetchUser: false,
        }
      }
    },

    redirect: {
      home: false,
    },
  },


  // Vuetify module configuration (https://go.nuxtjs.dev/config-vuetify)
  vuetify: {
    treeShake: true,
    customVariables: ['~/assets/css/variables.scss'],
    theme: {
      options: {
        customProperties: true
      },
      dark: false,
      themes: {
        light: {
          primary: '#007DC6',
          secondary: '#78B9E7',
          accent: '#FFC220',

          info: '#78B9E7',
          warning: '#F47321',
          error: colors.deepOrange.accent4,
          success: '#367C2B',
          background: '#F7F6F2',
          'dark-charcoal': '#303030',

        },
        dark: {
          primary: colors.blue.darken2,
          accent: colors.grey.darken3,
          secondary: colors.blue.darken3,
          info: colors.teal.lighten1,
          warning: colors.amber.base,
          error: colors.deepOrange.accent4,
          success: colors.green.accent3
        }
      }
    }
  },

  googleFonts: {
  families: {
      'Open+Sans': [400, 600],
      // 'EB+Garamond': {
      //   wght: [400, 600]
      // },
    },

    display: 'swap',
  },


  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    // "publicPath": "_nuxt/app",
    extend(config) {
      config.resolve.alias['shared'] = path.resolve(__dirname, './../shared');
      config.resolve.alias.shared = path.resolve(__dirname, "../shared");
    },

    // extractCSS: true,

    transpile: ["/shared/utils"],
    transpile: ["vee-validate/dist/rules"],
  }
}
