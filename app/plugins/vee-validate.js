import { extend } from "vee-validate";
import { required, email, integer, numeric, min_value, min } from "vee-validate/dist/rules";
import { setInteractionMode } from 'vee-validate';

setInteractionMode('eager');

extend("required", {
  ...required,
  message: "This field is required"
});

extend("email", {
  ...email,
  message: "A valid email is required"
});

extend("integer", {
  ...integer,
  message: "This field should be an integer"
});

extend("numeric", {
  ...numeric,
  message: "Alphanumeric characters required"
});

extend("min_value", {
  ...min_value,
  message: "Bigger then {min} please."
});


extend("min", {
  ...min,
  message: "At least {length} characters please."
});

extend('passwordConfirmation', {
  params: ['target'],
  validate(value, { target }) {
    return value == target;
  },
  message: 'Password confirmation does not match'
});
