import { safeNatNumber,   volumePriceStringToArr, productPriceForQty, } from 'shared/utils'


export const state = () => ({
  cartUIStatus: 'idle',
  items: [],
  cupon: null,
})

export const getters = {
  cartCount: (state) => {
    if(!state.items.length) return 0
    return state.items.reduce((acc, e) => acc + e.quantity, 0)
  },

  cartTotal: (state, getters) => {
    if(!state.cupon) return getters.cartSubtotal
    return (getters.cartSubtotal * (100 - getters.cuponDiscount)/100).toFixed(2)
  },

  cartSubtotal: (state) => {
    if(!state.items.length) return 0
    return state.items.reduce((acc, e) => acc + e.quantity * productPriceForQty(e.price, e.quantity, volumePriceStringToArr(e.volumePrice)), 0)
  },

  isCartEmpty: (state) => {
    return !state.items.length
  },

  itemCartQuantity: (state) => {
    return (itemId) => {
      let item = state.items.find(e => e._id === itemId)
      return item ? item.quantity : 0
    }
  },

  cuponDiscount: (state) => {
    return state.cupon ? state.cupon.discount : 0
  },

}

export const mutations = {
  updateCartUI: (state, payload) => {
    state.cartUIStatus = payload;
  },

  clearCart: (state) => {
    state.items = []
  },

  setCartitems: (state, payload) => {
    state.items = payload
  },

  addItemToCart: (state, payload) => {
    const { item } = payload
    let quantity = safeNatNumber(payload.quantity)
    const index = state.items.findIndex(e => e._id === item._id)
    if(index >= 0) {
      quantity = quantity + state.items[index].quantity
      state.items.splice(index, 1, { ...item, quantity })
    } else {
      state.items.push({ ...item, quantity })
    }
  },
  // item in cart max quantity = 1
  addSingularItemToCart: (state, payload) => {
    const { item } = payload
    let quantity = 1
    const index = state.items.findIndex(e => e._id === item._id)
    if(index >= 0) {
      state.items.splice(index, 1, { ...item, quantity })
    } else {
      state.items.push({ ...item, quantity })
    }
  },

  updateCartItem: (state, payload) => {
    const quantity = safeNatNumber(payload.quantity)
    const item = payload.item
    const index = state.items.findIndex(e => e._id === item._id)
    if(index < 0) return
    state.items.splice(index, 1, payload)
  },

  addOneItemToCart: (state, payload) => {
    const index = state.items.findIndex(e => e._id === payload._id)
    if(index < 0) return
    const { item, limitToOne } = payload
    const quantity = limitToOne ? 1 : state.items[index].quantity + 1
    const updated = { ...item, quantity }
    state.items.splice(index, 1, updated)
  },

  removeOneItemFromCart: (state, id) => {
    const index = state.items.findIndex(e => e._id === id)
    if(index < 0) return

    if(state.items[index].quantity <= 1) {
      state.items = state.items.filter(e => e._id !== id)
    } else {
      let updated = state.items[index]
      updated.quantity -= 1

      state.items.splice(index, 1, updated)
    }
  },

  removeItemFromCart: (state, id) => {
    state.items = state.items.filter(e => e._id !== id)
  },

  setCupon: (state, payload) => {
    state.cupon = payload
  },

  removeCupon: (state) => {
    state.cupon = null
  },
}
