export const state  = () => ({
  staticPaginationBlog: { // the paginations that was used when static site was generated
    itemsPerPage: process.env.itemsPerPage,
    sortBy: 'title',
    sortDesc: false,
    groupBy: [],
    groupDesc: [],
    multiSort: false,
    mustSort: false,
  },

  paginationBlog: { // the paginations that is used based on user criteria
    itemsPerPage: process.env.itemsPerPage,
    sortBy: 'title',
    sortDesc: false,
    groupBy: [],
    groupDesc: [],
    multiSort: false,
    mustSort: false,
  },

  staticPaginationShop: {
    itemsPerPage: 12,
    sortBy: 'name',
    sortDesc: false,
    groupBy: [],
    groupDesc: [],
    multiSort: false,
    mustSort: false,
  },

  paginationShop: {
    itemsPerPage: 12,
    sortBy: 'name',
    sortDesc: false,
    groupBy: [],
    groupDesc: [],
    multiSort: false,
    mustSort: false,
  },
})

export const mutations = {
  updateBlogPagOptions: (state, payload) => {
    state.paginationBlog = Object.assign({}, state.paginationBlog, payload)
  },

  updateShopPagOptions: (state, payload) => {
    state.paginationShop = Object.assign({}, state.paginationShop, payload)
  },
}
