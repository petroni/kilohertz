export const state = () => ({
  currecies: [
    {name: "Swiss Franc", code: "CHF", symbol: "CHF"},
    {name: "Euro", code: "EUR", symbol: "€"},
    {name: "US Dollar", code: "USD", symbol: "$"},
  ],

  currencyCode: "CHF",
})

const getters = {
  currency: (state) => state.currecies.find(e => e.code == state.currencyCode)
}
