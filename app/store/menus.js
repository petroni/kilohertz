export const state = () => ({
  mainMenu: [
    {title: "Shop", path: "/"},
    {title: "Galerie", path: "/galerie"},
    {title: "Referenzen", path: "/referenzen"},
    {title: "Kontakt", path: "/kontakt"},
  ],

  legalMenu: [
    {title: "Privacy Policy", path: "/legal/privacy-policy"},
    {title: "Terms and Conditions", path: "/legal/terms-and-conditions"},
  ],

})
