export const state = () => ({
  countries: [],
})

export const getters = {

}


export const mutations = {
  setCountries: (state, payload) => {
    state.countries = payload
  },
}


export const actions = {
  async loadCountries ({state, commit}, payload) {
    if(!state.countries.length) {
      try {
        let countries = await this.$axios.$get(`api/countries`)
        commit('setCountries', countries)
      } catch (error) {
        console.log('error %o', error)
      }
    }
  },
}
