export const state = () => ({
  billingAddress: {},
  loadedBillingAddress: false,
  content: [],
})

export const mutations = {
  setBillingAddress: (state, payload) => {
    state.billingAddress = payload
  },

  setLoaded: (state) => {
    state.loadedBillingAddress = true
  },

  cleanOrder (state) {
    state.billingAddress = {}
    state.loadedBillingAddress = false
  }
}

export const actions = {
  ensureBillingAddress: ({state, dispatch}, userId) => {
    if(!state.loadedBillingAddress) {
      dispatch('loadBillingAddress', userId)
    }
  },

  async loadBillingAddress ({state, commit}, userId) {
      if(!userId) return
      try {
        let { billingAddress = {} } = await this.$axios.$get(`/api/users/${userId}`)
        commit('setBillingAddress', billingAddress)
        commit('setLoaded')
      } catch (error) {
        console.log('loadBillingAddress error %o', error)
      }
  },

  updateBillingAddress: ({ commit }, payload) => {
    commit('setBillingAddress', payload)
  },

  checkoutRequest: ({commit}) => {
    commit('cleanOrder')
  },
}
