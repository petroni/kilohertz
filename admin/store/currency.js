export const state = () => ({
  currency: {
    name: 'Swiss Franc',
    code: 'CHF',
    symbol: "CHF"
  },

  currencies: [],
})

export const getters = {

}


export const mutations = {
  setCurrency: (state, payload) => {
    state.currency = payload
  },

  setCurrencies: (state, payload) => {
    state.currencies = payload
  },
}


export const actions = {
  async loadCurrencies ({state, commit}, payload) {
    if(!state.currencies.length) {
      let currencies = await this.$axios.$get('/api/currencies')
      commit('setCurrencies', currencies)
    }
  },
}
