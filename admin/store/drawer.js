export const state = () => ({
  open: true
})

export const mutations = {
  setDrawer (state, payload) {
    state.open = payload
  },

  toggleDrawer (state) {
    state.open = !state.open
  },

  close (state) {
    state.open = false
  },

  open (state) {
    state.open = true
  },
}
