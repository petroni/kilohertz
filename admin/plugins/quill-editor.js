import Vue from 'vue'
import VueQuillEditor from 'vue-quill-editor'
import { Quill } from 'vue-quill-editor'

import htmlEditButton from "quill-html-edit-button";

Quill.register({
  "modules/htmlEditButton": htmlEditButton
})

Vue.use(VueQuillEditor)
