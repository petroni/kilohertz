const path = require('path')
import colors from 'vuetify/es5/util/colors'

export default {
  // "srcDir": __dirname,
  // "buildDir": "_nuxt/admin",
  // "lambdaName": "admin", // if we don't name our secondary app, builds two index lambdas
  //
  // "router": {
  //   // gotta match our url routing at the app level
  //   "base": "/admin/"
  // },

  // Global page headers: https://go.nuxtjs.dev/config-head

  head: {
    titleTemplate: '%s - admin',
    title: 'Kilohertz Admin',
    htmlAttrs: {
      lang: 'de'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },

  env: {
    locale: process.env.LOCALE || 'de-CH',
    API_URL: process.env.API_URL,
    IMGKIT_PUBLIC_KEY: 'public_+9disIZyM7bZUFcu7H1CfrIcSsU=',
    IMGKIT_ENDPOINT: 'https://ik.imagekit.io/kilohertz',
    IMGKIT_LOCAL_SERVER_AUTH_PATH: '/api/media-auth/imagekit',
    DEFAULT_SESSION_SKU: 'GGD',
  },

  server: {
    port: 5001,
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    // for Quill Editor (https://quilljs.com/)
    'quill/dist/quill.core.css',
    // for snow theme
    'quill/dist/quill.snow.css',

    'chartist/dist/chartist.min.css',
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    "~/plugins/vee-validate.js",
    { src: '~/plugins/quill-editor', mode: 'client' },
    "~/plugins/vue-chartist.js",
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: [
    '~/components/',
    '~/../shared/components',
  ],

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/vuetify
    '@nuxtjs/vuetify',
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
    '@nuxtjs/auth',
  ],

  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {
    proxy: true,
  },

  proxy: {
    '/api': {
      target: process.env.API_URL,
    }
  },

  // Vuetify module configuration (https://go.nuxtjs.dev/config-vuetify)
  vuetify: {
    customVariables: ['~/assets/variables.scss'],
    theme: {
      dark: false,
      themes: {
        dark: {
          primary: colors.blue.darken2,
          accent: colors.grey.darken3,
          secondary: colors.amber.darken3,
          info: colors.teal.lighten1,
          warning: colors.amber.base,
          error: colors.deepOrange.accent4,
          success: colors.green.accent3
        }
      }
    }
  },

  auth: {
    strategies: {
      local: {
        endpoints: {
          login: { url: '/api/auth/login', method: 'post', propertyName: 'token' },
          logout: false,
          user: { url: '/api/auth/me', method: 'get', propertyName: 'user' }
        },
        // tokenRequired: true,
        // tokenType: 'bearer',
        // globalToken: true,
        // autoFetchUser: true
      },
    },

    redirect: {
      logout: '/login'
    },
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    // "publicPath": "_nuxt/admin",
    extend(config) {
    //  config.resolve.alias['shared'] = path.resolve(__dirname, './../common')
      config.resolve.alias['shared'] = path.resolve(__dirname, './../shared');
      config.resolve.alias.shared = path.resolve(__dirname, "../shared");
    },

    transpile: ["/shared/utils"],
    transpile: ["vee-validate/dist/rules"],
  }
}
