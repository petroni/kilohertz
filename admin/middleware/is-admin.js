export default function ({$auth, redirect, route}) {
  if(!$auth.loggedIn || $auth.user.role != 'admin'){
    console.log('is admin redirect route.path %o', route.path)
    if(route.path != '/login') redirect('/login')
  }
}
